Stock-service

Spring Boot приложение для хранения информации о товарах с подключением к базе данных PostgreSQL,
и уведомлением других серверов об изменении товаров через брокер Apache Kafka.

Сервис хранит: 
Информацию о товарах: id, цена, название, описание, id категории, количество, производитель.
Информацию о категориях: id, название.
Информацию о зарезервированном товаре: id, id продукта, количество, id пользователя   
Позволяет создавать, редактировать, резервировать, удалять товары, получать информацию о товарах. 
Так же имеет функционал по управлению категориями товаров и возможности для управления резервами товаров.
Подключение к базе данных происходит через Hibernate. Структура базы данных создается при помощи Liquibase. 
Данный сервис использует программного брокера сообщений Apache Kafka для оперативного уведомления об удалении,
добавлении, изменении состояния товара, а так же для поставки информации об изменении цены в price-service.

API сервиса:

- Создание нового продукта: POST http://localhost:8082/stock/item/create
    запрос должен содержать класс Product без id(генерируется stock-service), пример:
- {"name": "Дыня",  "description": "1 сорт",  "manufacturer": "FruitCorp",  "category": "2", "quantity": "15",  "price": "8"
- Обновление продукта: PUT http://localhost:8082/stock/item/update
    запрос должен содержать объект Product, пример:
    {"id": "14", "name": "Мандарин", "description": "спелый","manufacturer": "OrangeCorp", "category": "2", "quantity": "15", "price": "500.0"}
- Удаление продукта по id: DELETE http://localhost:8082/stock/delete?id={id}
- Получение продукта по id: GET http://localhost:8082/stock/item?id={id}
- Получение всех продуктов: GET http://localhost:8082/stock/item/all
- Получение продуктов по категории: GET http://localhost:8082/stock/item/all/byCategory?categoryId={categoryId}
- Получение списка категорий: GET http://localhost:8082/stock/item/allCategory
- Создание резерва: POST http://localhost:8082/stock/item/reserve
    запрос должен содержать объект Reserve, пример:
    {"productId": "2", "quantity": "1", "userId": "16"}
- Удаление резерва по id: DELETE http://localhost:8082/stock/item/unreserve?reserveId={reserveId}
- Получение списка всех резервов GET http://localhost:8082/stock/reserve/get/all
- Получение списка резервов по id продукта: GET http://localhost:8082/stock/reserve/get/all/byProductId?productId={productId}
