CREATE
OR REPLACE FUNCTION tr_change_product()
  RETURNS trigger AS
$$
DECLARE
dml_type varchar(1);
created_by
varchar(50);
BEGIN
case
		when TG_OP = 'UPDATE' then dml_type := 'U';
when TG_OP = 'DELETE' then dml_type := 'D';
end
case;
	created_by
:= current_user;
INSERT INTO products_arch(product_id, name, description, manufacturer, category, quantity, dml_type, created_by)
VALUES (old.id, old.name, old.description, old.manufacturer, old.category, old.quantity, dml_type, created_by);

RETURN new;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER tr_change_product
    AFTER update or
delete
ON products
   FOR EACH ROW
  EXECUTE PROCEDURE tr_change_product()