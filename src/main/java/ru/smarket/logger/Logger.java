package ru.smarket.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Aspect
@Component
public class Logger {

    @Pointcut("execution(* ru.smarket.controller.StockController.*(..))")
    public void stockController() {
    }

    @Before("stockController()")
    public void beforeMetod(JoinPoint joinPoint) throws IOException {
        List<String> arg = getArgs(joinPoint);
        writeLog(LocalDateTime.now() + " started Method: " + joinPoint.getSignature().getName() + arg);
    }

    @AfterReturning("stockController()")
    public void afterMetod(JoinPoint joinPoint) throws IOException {
        List<String> arg = getArgs(joinPoint);
        writeLog(LocalDateTime.now() + " finished Method: " + joinPoint.getSignature().getName() + arg);
    }

    @AfterThrowing(value = "stockController()", throwing = "ex")
    public void afterExeption(JoinPoint joinPoint, Throwable ex) throws IOException {
        List<String> arg = getArgs(joinPoint);
        String message = String.format("%s ERROR Method: %s %s %s", LocalDateTime.now(), joinPoint.getSignature().getName(), arg, ex);
        writeLog(message);
    }

    public void writeLog(String str)
            throws IOException {
        /**
         * create directory "log" if not exists
         */
        Path logDir = Paths.get(Paths.get("").toAbsolutePath().toString() + "/log/");
        if (!Files.exists(logDir)) {
            try {
                Files.createDirectory(logDir);
            } catch (IOException e) {
                System.err.println(e);
            }
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("log/log.txt", true))) {
            writer.write(str);
            writer.newLine();
        }
    }

    /**
     * get arguments from method
     */
    private List<String> getArgs(JoinPoint joinPoint) {
        List<String> args = new ArrayList<>();
        for (int i = 0; i < joinPoint.getArgs().length; i++) {
            Object argValue = joinPoint.getArgs()[i];
            args.add("arg." + i + "=" + argValue);
        }
        return args;
    }
}
