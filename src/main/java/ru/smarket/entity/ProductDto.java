package ru.smarket.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDto {
    private int id;
    private String name;
    private String description;
    private String manufacturer;
    private int category;
    private int quantity;
    private BigDecimal purchasePrice;
}