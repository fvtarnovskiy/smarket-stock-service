package ru.smarket.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "reserve")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Reserve {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "product_id")
    private int productId;
    @Column(name = "quantity")
    private int quantity;

    public Reserve(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }
}
