package ru.smarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smarket.entity.Product;
import ru.smarket.entity.ProductDto;
import ru.smarket.repository.ProductRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ProductServiceImp implements ProductService {

    SendService sendService;
    ProductRepository productRepository;

    @Autowired
    public ProductServiceImp(SendService sendService, ProductRepository productRepository) {
        this.sendService = sendService;
        this.productRepository = productRepository;
    }

    @Override
    public Optional<Product> getById(int id) {
        if (!productRepository.existsById(id)) {
            throw new NoSuchElementException("[ERROR] Product with id=" + id + " not found!");
        }
        return productRepository.findById(id);
    }

    @Override
    public List<Product> getByCategory(int categoryId) {
        return productRepository.findByCategory(categoryId);
    }

    @Override
    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    @Override
    public void create(ProductDto product) throws IllegalArgumentException {
            // Check price
            sendService.priceValidator(product.getPurchasePrice());
            // Сохраняем продукт в базу данных
        Product newProduct = new Product();
        newProduct.setCategory(product.getCategory());
        newProduct.setQuantity(product.getQuantity());
        newProduct.setDescription(product.getDescription());
        newProduct.setId(product.getId());
        newProduct.setManufacturer(product.getManufacturer());
        newProduct.setName(product.getName());
            Product createdProduct = productRepository.save(newProduct);
            // Уведомляем price-service
            sendService.sendPriceChange(createdProduct.getId(), "CREATE", product.getPurchasePrice());
            // Отправляем уведомление в кафку о создании продукта
            sendService.sendProductChange("created", createdProduct);
    }

    @Override
    public void update(Product product) throws IllegalArgumentException {
        if (!productRepository.existsById(product.getId())) {
            throw new IllegalArgumentException("[ERROR] Product not found!");
        } else {
            // Check price
            sendService.priceValidator(product.getPrice());
            // Обновляем запись в базе данных
            Product updatedProduct = productRepository.save(product);
            // Уведомляем price-service (цена не хранится в БД, используем цену из запроса)
            sendService.sendPriceChange(updatedProduct.getId(), "UPDATE", product.getPrice());
            // Отправляем уведомление в кафку об изменении продукта
            sendService.sendProductChange("updated", updatedProduct);
        }
    }

    @Override
    public void delete(int itemId) {
        // Удаляем продукт из базы данных
        if (!productRepository.existsById(itemId)) {
            throw new NoSuchElementException("[ERROR] Product with id " + itemId + " doesn't exist!");
        } else {
            Optional<Product> deletedProduct = productRepository.findById(itemId);
            productRepository.deleteById(itemId);
            // Уведомляем price-service, для операции DELETE цена убирается внутренней проверкой .sendPriceChange()
            sendService.sendPriceChange(deletedProduct.get().getId(), "DELETE", null);
            // Отправляем уведомление в кафку об удалении продукта
            sendService.sendProductChange("deleted", deletedProduct.get());
        }
    }
}
