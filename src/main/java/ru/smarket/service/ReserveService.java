package ru.smarket.service;

import ru.smarket.entity.Reserve;

import java.nio.file.AccessDeniedException;
import java.util.List;

public interface ReserveService {

    void setReserve(Reserve reserve);
    void unreserve(int reserveId);
    List<Reserve> getReservesByProductId(int productId);
    List<Reserve> getReserves();
}
