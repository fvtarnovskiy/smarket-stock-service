package ru.smarket.service;

import ru.smarket.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getCategories();
}
