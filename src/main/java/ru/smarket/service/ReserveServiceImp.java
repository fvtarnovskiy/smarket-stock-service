package ru.smarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smarket.entity.Product;
import ru.smarket.entity.Reserve;
import ru.smarket.repository.ProductRepository;
import ru.smarket.repository.ReserveRepository;

import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class ReserveServiceImp implements ReserveService {

    ReserveRepository reserveRepository;
    ProductRepository productRepository;

    @Autowired
    public ReserveServiceImp(ReserveRepository reserveRepository, ProductRepository productRepository) {
        this.reserveRepository = reserveRepository;
        this.productRepository = productRepository;
    }

    @Override
    public void setReserve(Reserve reserve) {

        // Проверка наличия резервируемого продукта в БД
        if (!productRepository.existsById(reserve.getProductId())) {
            throw new IllegalArgumentException("[ERROR][ReserveServiceImp] Error creating reserve - product doesn't exist!");
        }
        // Проверка резервируемого количества
        if (reserve.getQuantity() <= 0) {
            throw new IllegalArgumentException("[ERROR][ReserveServiceImp] Trying to reserve zero or negative amount of products");
        }
        // Проверка доступного количества для резерва:
        // Собираем резервы по продукту, который хотим зарезервировать
        Stream<Reserve> reserveStream = reserveRepository.findAllByProductId(reserve.getProductId()).stream();
        // Вычисляем количество зарезервированного продукта
        int alreadyReservedAmount = reserveStream.mapToInt(Reserve::getQuantity).sum();
        // Выкидываем исключение, если доступное количество меньше суммы резервируемого и уже зарезервированного количества
        Optional<Product> product = productRepository.findById(reserve.getProductId());
        Product product1 = product.get();
        if (product1.getQuantity() < (reserve.getQuantity() + alreadyReservedAmount)) {
            throw new IllegalArgumentException("[ERROR][ReserveServiceImp] Not enough products available to reserve");
        }

        product1.setQuantity(product1.getQuantity() - reserve.getQuantity());

        productRepository.save(product1);
        reserveRepository.save(reserve);
        System.out.println("[ReserveServiceImp] Reserve saved: " + reserve);
    }

    @Override
    public void unreserve(int reserveId){

        if (!reserveRepository.existsById(reserveId)) {
            throw new NoSuchElementException("[ERROR] Reserve with id " + reserveId + " doesn't exist!");
        }

        Optional<Reserve> deletedReserve = reserveRepository.findById(reserveId);

        // Удаляем резерв из базы данных
        reserveRepository.deleteById(reserveId);
        System.out.println("[ReserveServiceImp] Reserve deleted: " + deletedReserve);
    }

    @Override
    public List<Reserve> getReserves() {
        return reserveRepository.findAll();
    }

    @Override
    public List<Reserve> getReservesByProductId(int productId) {
        return reserveRepository.findAllByProductId(productId);
    }

}
