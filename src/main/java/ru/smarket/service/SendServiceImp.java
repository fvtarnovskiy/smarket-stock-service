package ru.smarket.service;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.smarket.entity.Product;
import ru.smarket.jms.Producer;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class SendServiceImp implements SendService {

    private final Producer producer;

    @Autowired
    public SendServiceImp(Producer producer) {
        this.producer = producer;
    }

    @Override
    public void sendPriceChange(int id, String action, BigDecimal price) {

        JSONObject message = new JSONObject();
        message.put("id", id);
        message.put("operation", action);
        // Для любой операции кроме DELETE включаем цену в сообщение
        if (!"DELETE".equalsIgnoreCase(action)) {
            message.put("price", price);
        }

        System.out.println("[ProductServiceImpl] Sending message to topic '" + PRICE_CHANGE_TOPIC + "' message=" + message);
        producer.sendMessage(PRICE_CHANGE_TOPIC, message.toJSONString());
    }

    @Override
    public void sendProductChange(String action, Product product) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        StringBuilder message = new StringBuilder("[")
                .append(LocalDateTime.now().format(format))
                .append("] Product ")
                .append(action)
                .append(", ")
                .append(product.toString());

        System.out.println("[ProductServiceImpl] Sending message to topic '" + PRODUCT_CHANGE_TOPIC + "' message=" + message);
        producer.sendMessage(PRODUCT_CHANGE_TOPIC, message.toString());
    }

    @Override
    public void priceValidator(BigDecimal price) {
        // Check price isn't null
        try {
            if (price == null) {
                throw new IllegalArgumentException("[ERROR] Price not found!");
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error message: " + e.getMessage());
            return;
        }
        // Check price isn't negative or null
        try {
            if (price.compareTo(BigDecimal.ZERO) != 1) {
                throw new IllegalArgumentException("[ERROR] Price is negative or  zero! price=" + price);
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error message: " + e.getMessage());
            return;
        }
    }
}
