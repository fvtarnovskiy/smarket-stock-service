package ru.smarket.service;

import ru.smarket.entity.Product;
import ru.smarket.entity.ProductDto;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Optional<Product> getById(int id);

    List<Product> getByCategory(int categoryId);

    List<Product> getProducts();

    void create(ProductDto product);

    void update(Product product);

    void delete(int id);
}
