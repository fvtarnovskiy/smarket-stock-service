package ru.smarket.service;

import ru.smarket.entity.Product;

import java.math.BigDecimal;

public interface SendService {
    String PRODUCT_CHANGE_TOPIC = "productChange";
    String PRICE_CHANGE_TOPIC = "priceChange";

    void sendProductChange(String action, Product product);

    void sendPriceChange(int id, String action, BigDecimal price);

    void priceValidator(BigDecimal price);
}
