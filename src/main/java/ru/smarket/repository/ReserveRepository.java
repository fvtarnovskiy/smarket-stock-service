package ru.smarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.smarket.entity.Product;
import ru.smarket.entity.Reserve;

import java.util.List;

@Repository
public interface ReserveRepository extends JpaRepository<Reserve, Integer> {

    List<Reserve> findAllByProductId(int category);

}
