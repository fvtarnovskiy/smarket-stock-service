package ru.smarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.smarket.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
