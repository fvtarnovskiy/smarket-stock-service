package ru.smarket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.smarket.entity.Category;
import ru.smarket.entity.Product;
import ru.smarket.entity.ProductDto;
import ru.smarket.entity.Reserve;
import ru.smarket.service.CategoryService;
import ru.smarket.service.ProductService;
import ru.smarket.service.ReserveService;

import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping(value = "/stock")
public class StockController {

    ProductService productService;

    @Autowired
    private void setProductService(ProductService productService) {
        this.productService = productService;
    }

    /**
     * Запрос продукта по id
     *
     * @param id - id продукта в stock-service
     * @return - Product в формате jSON + status code 200;
     * status code 404 если объект не найден в базе данных
     */
    @GetMapping(value = "/item", produces = "application/json;charset=UTF-8")
    public ResponseEntity<Optional<Product>> getById(@RequestParam int id) {
        try {
            return new ResponseEntity<>(productService.getById(id), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    /**
     * Запрос всех продуктов по категории
     *
     * @param categoryId - id категории
     * @return JSON-массив объектов {@link Product} по категории + status code 200
     */
    @GetMapping(value = "/item/all/byCategory", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Product>> getByCategory(@RequestParam int categoryId) {
        return new ResponseEntity<>(productService.getByCategory(categoryId), HttpStatus.OK);
    }

    /**
     * Запрос всех продуктов
     *
     * @return JSON-массив всех {@link Product} + status code 200
     */
    @GetMapping(value = "/item/all", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Product>> getProducts() {
        return new ResponseEntity<>(productService.getProducts(), HttpStatus.OK);
    }

    /**
     * Запрос на создание нового продукта
     *
     * @param product - объект {@link Product} без id
     * @return - status code 201 если {@link Product} создан;
     * status code 400 если запрос содержит некорректные параметры или был передан id
     */
    @PostMapping("/item/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody ProductDto product) {
        try {
            productService.create(product);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    /**
     * Запрос на обновление существующего продукта
     *
     * @param product - объект {@link Product}
     * @return status code 200 если {@link Product} успешно обновлен;
     * status code 400 при попытке обновить несуществующий объект
     */
    @PutMapping("/item/update")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody Product product) {
        try {
            productService.update(product);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    /**
     * Запрос на удаление продукта
     *
     * @param id - id {@link Product} который необходимо удалить
     * @return - status code 200 при успешном удалении продукта;
     * status code 404 если {@link Product} не удалось найти в БД
     */
    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam int id) {
        try {
            productService.delete(id);
        } catch (NoSuchElementException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    /**
     * Добавляем зависимость CategoryService
     */
    CategoryService categoryService;

    @Autowired
    private void setCategoryRepository(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * Запрос списка категорий
     *
     * @return JSON-массив {@link Category}
     */
    @GetMapping(value = "/item/allCategory", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Category>> getCategories() {
        return new ResponseEntity<>(categoryService.getCategories(), HttpStatus.OK);
    }

    /**
     * добавляем зависимость ReserveService
     */
    ReserveService reserveService;

    @Autowired
    private void setReserveService(ReserveService reserveService) {
        this.reserveService = reserveService;
    }

    /**
     * Создание резерва
     *
     * @param reserve - объект {@link Reserve}
     * @return status code - 200 если резерв успешно создан;
     * status code 400 если параметры запроса некорректны или
     * недостаточно доступных продуктов для создания нового резерва
     */
    @PostMapping(value = "/item/reserve")
    @ResponseStatus(HttpStatus.OK)
    public void setReserve(@RequestBody Reserve reserve) {
        try {
            reserveService.setReserve(reserve);
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    /**
     * Удаление резерва
     *
     * @param reserveId - id {@link Reserve}
     * @return status code 200 если резерв успешно удален;
     * 404 - если {@link Reserve} с указанным id не найден
     */
    @DeleteMapping(value = "/item/unreserve")
    @ResponseStatus(HttpStatus.OK)
    public void unreserve(@RequestParam int reserveId) {
        try {
            reserveService.unreserve(reserveId);
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        }
    }

    /**
     * Запрос списка всех резервов
     *
     * @return JSON-массив {@link Reserve}
     */
    @GetMapping(value = "/reserve/get/all", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Reserve>> getReserves() {
        return new ResponseEntity<>(reserveService.getReserves(), HttpStatus.OK);
    }


    /**
     * Запрос резервов по определенному продукту
     *
     * @param productId - id {@link Product}
     * @return JSON-массив {@link Reserve} + status code 200
     */
    @GetMapping(value = "/reserve/get/all/byProductId", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<Reserve>> getReservesByProductId(@RequestParam int productId) {
        return new ResponseEntity<>(reserveService.getReservesByProductId(productId), HttpStatus.OK);
    }
}