package ru.smarket.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.kafka.core.KafkaTemplate;
import ru.smarket.entity.Product;
import ru.smarket.repository.ProductRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SendServiceImpTest {
    private SendServiceImp sendServiceImp;
    private ProductRepository productRepository;
    private ProductServiceImp productServiceImp;
    private Product product;
    private KafkaTemplate<String, String> kafkaTemplate;

    private Product oneProductTest = new Product(5, "testName", "testDescription",
            "testManufacturer", 1, 10, BigDecimal.valueOf(50));
    private Product ProductTest = new Product();

    @BeforeEach
    void setUp() {
        productRepository = mock(ProductRepository.class);
        kafkaTemplate = mock(KafkaTemplate.class);
        productServiceImp = new ProductServiceImp(sendServiceImp, productRepository);
        product = new Product(5, "testName", "testDescription",
                "testManufacturer", 1, 10, BigDecimal.valueOf(50));

        when(productRepository.save(any())).thenReturn(product);
    }

    @Test
    @Disabled
    void testSendProductChange() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        productServiceImp.update(product);
        Mockito.verify(kafkaTemplate, Mockito.times(1)).send("productChange",
                "[" + LocalDateTime.now().format(format) + "] Product created, " +
                        "Product{id=5, name='testName', description='testDescription', " +
                        "manufacturer='testManufacturer', category=1, quantity=10}");
    }

    @Test
    @Disabled
    void testPriceValidator() {
        sendServiceImp.priceValidator(new BigDecimal((char[]) null));
        //sendServiceImp.priceValidator(BigDecimal.valueOf(-50));
    }

}