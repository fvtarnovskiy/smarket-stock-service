package ru.smarket.service;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.smarket.entity.Category;
import ru.smarket.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoryServiceImpTest {

    private CategoryRepository categoryRepository;
    private CategoryServiceImp categoryServiceImp;
    private Category category;
    private List<Category> categoryList;

    @BeforeEach
    void setUp() {
        categoryRepository = mock(CategoryRepository.class);
        categoryServiceImp = new CategoryServiceImp(categoryRepository);
        categoryList = new ArrayList<>();
        categoryList.add(new Category(1, "Консервы"));
        categoryList.add(new Category(2, "Молочные продукты"));
        when(categoryRepository.findAll()).thenReturn(categoryList);
    }

    @Test
    void testGetCategories() {
        assertEquals(categoryList.size(), categoryServiceImp.getCategories().size());
        assertTrue(categoryList.equals(categoryServiceImp.getCategories()));
        verify(categoryRepository, Mockito.times(2)).findAll();
    }
}