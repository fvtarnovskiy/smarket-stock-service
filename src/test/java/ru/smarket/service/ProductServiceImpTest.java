package ru.smarket.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.smarket.entity.Product;
import ru.smarket.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductServiceImpTest {
    private ProductRepository productRepository;
    private ProductServiceImp productServiceImp;
    private SendServiceImp sendServiceImp;
    private Product product;
    private List<Product> productList;

    private Product oneProductTest = new Product(5, "testName", "testDescription",
            "testManufacturer", 1, 10, BigDecimal.valueOf(50));

    @BeforeEach
    void setUp() {
        productRepository = mock(ProductRepository.class);
        productServiceImp = new ProductServiceImp(sendServiceImp, productRepository);
        product = new Product(5, "testName", "testDescription",
                "testManufacturer", 1, 10, BigDecimal.valueOf(50));
        productList = new ArrayList<>();
        productList.add(product);
        productList.add(product);

        when(productRepository.save(any())).thenReturn(product);
        when(productRepository.findById(anyInt())).thenReturn(Optional.ofNullable(product));
        when(productRepository.findAll()).thenReturn(productList);
        when(productRepository.findByCategory(anyInt())).thenReturn(productList);
    }

    @Test
    void testGetById() {
        assertEquals(oneProductTest.getId(), productServiceImp.getById(5).get().getId());
        assertEquals(oneProductTest.getName(), productServiceImp.getById(5).get().getName());

        Mockito.verify(productRepository, Mockito.times(2)).findById(5);
    }

    @Test
    void testGetByCategory() {
        assertEquals(productList.size(), productServiceImp.getByCategory(1).size());
        Mockito.verify(productRepository, Mockito.times(1)).findByCategory(1);
    }

    @Test
    void testGetProducts() {
        assertEquals(productList.size(), productServiceImp.getProducts().size());
        assertTrue(productList.equals(productServiceImp.getProducts()));
        Mockito.verify(productRepository, Mockito.times(2)).findAll();
    }

    @Test
    void testCreate() {
        assertEquals(oneProductTest.getId(), productRepository.save(product).getId());

        productRepository.save(oneProductTest);
        Mockito.verify(productRepository, Mockito.times(1)).save(oneProductTest);
    }

    @Test
    void testUpdate() {
        productServiceImp.update(product);
        assertEquals(oneProductTest.getId(), productRepository.save(product).getId());

        productRepository.save(oneProductTest);
        Mockito.verify(productRepository, Mockito.times(1)).save(oneProductTest);
    }

    @Test
    void testDelete() {
        productServiceImp.delete(oneProductTest.getId());

        productRepository.deleteById(oneProductTest.getId());
        Mockito.verify(productRepository, Mockito.times(1)).deleteById(oneProductTest.getId());
    }
}