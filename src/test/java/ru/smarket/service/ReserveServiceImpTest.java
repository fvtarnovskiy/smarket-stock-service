//package ru.smarket.service;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Disabled;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//import ru.smarket.entity.Reserve;
//import ru.smarket.repository.ProductRepository;
//import ru.smarket.repository.ReserveRepository;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.anyInt;
//import static org.mockito.Mockito.*;
//
//class ReserveServiceImpTest {
//
//    private ReserveRepository reserveRepository;
//    private ProductRepository productRepository;
//    private ReserveServiceImp reserveServiceImp;
//    private Reserve reserve;
//    private List<Reserve> reserveList;
//
//    @BeforeEach
//    void setUp() {
//        reserveRepository = mock(ReserveRepository.class);
//        productRepository = mock(ProductRepository.class);
//        reserve = new Reserve(1, 5, 2);
//        reserveList = new ArrayList<>();
//        reserveList.add(reserve);
//        reserveList.add(reserve);
//
//        when(reserveRepository.save(reserve)).thenReturn(reserve);
//        when(reserveRepository.findAll()).thenReturn(reserveList);
//        when(reserveRepository.findAllByUserId(anyInt())).thenReturn(reserveList);
//        when(reserveRepository.findAllByProductId(anyInt())).thenReturn(reserveList);
//    }
//
//    @Test
//    @Disabled
//    void testSetReserve() {
//        reserveServiceImp.setReserve(reserve);
//        reserveServiceImp.reserveRepository.save(reserve);
//
//        assertEquals(reserve.getId(), reserveRepository.save(reserve).getId());
//        Mockito.verify(reserveRepository, Mockito.times(2)).save(reserve);
//    }
//
//    @Test
//    @Disabled
//    void testGetReserves() {
//        assertEquals(reserveList.size(), reserveServiceImp.getReserves().size());
//        assertTrue(reserveList.equals(reserveServiceImp.getReserves()));
//        verify(reserveRepository, Mockito.times(2)).findAll();
//    }
//
//    @Test
//    @Disabled
//    void testGetReservesByUserId() {
//        assertEquals(reserveList.size(), reserveServiceImp.getReservesByUserId(2).size());
//        assertTrue(reserveList.equals(reserveServiceImp.getReservesByUserId(2)));
//        verify(reserveRepository, Mockito.times(2)).findAllByUserId(2);
//    }
//
//    @Test
//    @Disabled
//    void testGetReservesByProductId() {
//        assertEquals(reserveList.size(), reserveServiceImp.getReservesByProductId(1).size());
//        assertTrue(reserveList.equals(reserveServiceImp.getReservesByProductId(1)));
//        verify(reserveRepository, Mockito.times(2)).findAllByProductId(1);
//    }
//}